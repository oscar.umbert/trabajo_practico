package com.logica;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
public class Tablero {
	private Integer[][] matriz;
	private List<Integer> filaVacia;
	private List<Integer> columnaVacia;
	private int unionesMovimiento;
	private int puntosMovimiento;
	private int contadorMovimiento;
	private int indiceVacio;
	private final int num = 2048;
	private boolean hayCambios;
	private boolean gano = false;
	
    Tablero() {
		super();
		matriz = OperacionesMatriz.crearMatriz();
		hayCambios = true;
	}
	Tablero(Integer[][] matriz) {
		super();
		this.matriz = matriz;
		hayCambios = true;
	}
	/**
	 * 
	 * @return
	 */
	int getPuntosMovimiento() {
		return puntosMovimiento;
	}
	/**
	 * 
	 * @param puntosMovimiento
	 */
	int getUnionesMovimiento() {
		return unionesMovimiento;
	}
	Integer[][] getMatriz() {
		return matriz;
	}
    boolean getGano() {
    	return this.gano;
    }
    boolean hayCambios() {
    	return hayCambios;
    }
	private void moverColumnaArriba(int columna){
		boolean terminar = false;
		boolean cero = false;
		int indiceCero = 0;
		int i = 0;
		while(!terminar) {
			if(matriz[i][columna].equals(0)) {
				if(!cero) {
					cero = true;
					indiceCero = i;
				}
			}else {
				if(cero) {
					matriz[indiceCero][columna]=matriz[i][columna];
					matriz[i][columna]=0;
					i = 0;
					cero = false;
					indiceCero = i;
					contadorMovimiento++;
				}
			}			
			i++;
			if(matriz.length == i) {
				terminar = true;
			}
							
		}
	}
	private void sumarColumnaArriba(int columna){
		boolean terminar = false;
		int i = 0;
		while(!terminar) {
			
			if(matriz[i][columna].equals(matriz[i+1][columna]) && !matriz[i][columna].equals(0)) {
				matriz[i][columna] = matriz[i][columna]+matriz[i][columna];
				matriz[i+1][columna] = 0;
				unionesMovimiento++;
				puntosMovimiento = puntosMovimiento + matriz[i][columna];
				
				if(matriz[i][columna].equals(num)) {
					gano = true;
				}
			}
			i++;
			if(matriz.length-1 == i) {
				terminar = true;
			}
			
		}
	}
	private void moverColumnaAbajo(int columna){
		boolean terminar = false;
		boolean cero = false;
		int indiceCero = 0;
		int i = matriz.length-1;
		while(!terminar) {
			if(matriz[i][columna].equals(0)) {
				if(!cero) {
					cero = true;
					indiceCero = i;
				}
			}else {
				if(cero) {
					matriz[indiceCero][columna]=matriz[i][columna];
					matriz[i][columna] = 0;
					i = matriz.length-1;
					cero = false;
					indiceCero = i;
					contadorMovimiento++;
				}
			}
			
			i--;
			if(-1 == i) {
				terminar = true;
			}
					
					
		}
	}
	private void sumarColumnaAbajo(int columna){
		boolean terminar = false;
		int i = matriz.length-1;
		while(!terminar) {
			
			if(matriz[i][columna].equals(matriz[i-1][columna]) && !matriz[i][columna].equals(0)) {
				matriz[i][columna] = matriz[i][columna]+matriz[i][columna];
				matriz[i-1][columna] = 0;
				unionesMovimiento++;
				puntosMovimiento = puntosMovimiento + matriz[i][columna];
				if(matriz[i][columna].equals(num)) {
					gano = true;
				}
			}
			
			
			if(1 == i) {
				terminar = true;
			}
			i--;
			
		}
	}
	private void sumarFilaIzq(int fila){
		boolean terminar = false;
		int i = 0;
		while(!terminar) {
			
			if(matriz[fila][i].equals(matriz[fila][i+1]) && !matriz[fila][i].equals(0)) {
				matriz[fila][i] = matriz[fila][i]+matriz[fila][i];
				matriz[fila][i+1] = 0;
				unionesMovimiento++;
				puntosMovimiento = puntosMovimiento + matriz[fila][i];
				if(matriz[fila][i].equals(num)) {
					gano = true;
				}
			}
			
			if(matriz.length-2 == i) {
				terminar = true;
			}else {
				i++;
			}
			
			
		}
				
	}
	private void moverFilaIzq(int fila){
		boolean terminar = false;
		boolean cero = false;
		int indiceCero = 0;
		int i = 0;
		while(!terminar) {
			if(matriz[fila][i].equals(0)) {
				if(!cero) {
					cero = true;
					indiceCero = i;
				}
			}else {
				if(cero) {
					matriz[fila][indiceCero]=matriz[fila][i];
					matriz[fila][i]=0;
					contadorMovimiento++;
					i = 0;
					cero = false;
					indiceCero = i;
				}
			}
			
			i++;
			if(matriz.length == i) {
				terminar = true;
			}
					
					
		}
	}
	private void moverFilaDer(int fila){
		boolean terminar = false;
		boolean cero = false;
		int indiceCero = 0;
		int i = matriz.length-1;
		while(!terminar) {
			if(matriz[fila][i].equals(0)) {
				if(!cero) {
					cero = true;
					indiceCero = i;
				}
			}else {
				if(cero) {
					matriz[fila][indiceCero]=matriz[fila][i];
					matriz[fila][i]=0;
					contadorMovimiento++;
					i = matriz.length-1;
					cero = false;
					indiceCero = i;
				}
			}
			i--;
			if(-1 == i) {
				terminar = true;
			}			
		}	
	}
	private void sumarFilaDer(int fila){
		boolean terminar = false;
		int i = matriz.length-1;
		while(!terminar) {
			
			if(matriz[fila][i].equals(matriz[fila][i-1]) && !matriz[fila][i].equals(0)) {
				matriz[fila][i] = matriz[fila][i]+matriz[fila][i];
				matriz[fila][i-1] = 0;
				unionesMovimiento++;
				puntosMovimiento = puntosMovimiento + matriz[fila][i];
				if(matriz[fila][i].equals(num)) {
					gano = true;
				}
			}
			
			if(1 == i) {
				terminar = true;
			}else {
				i--;
			}
			
		}
	}
	private void movimientoIzq(int fila){
		
		moverFilaIzq(fila);
		sumarFilaIzq(fila);
		moverFilaIzq(fila);	
	}
	private void movimientoDer(int fila){
		
		moverFilaDer(fila);
		sumarFilaDer(fila) ;
		moverFilaDer(fila) ;
	}
	private void movimientoArriba(int columna){
		
		moverColumnaArriba(columna);
		sumarColumnaArriba(columna);
		moverColumnaArriba(columna);
	}
	private void movimientoAbajo(int columna){
		
		moverColumnaAbajo(columna);
		sumarColumnaAbajo(columna);
		moverColumnaAbajo(columna);
	}
	void calcularMovimiento(int sentido) {
		
		inicializarVariables();
		
		for(int i = 0;i<matriz.length;i++) {
			switch (sentido) {
			    case 0: 
			    	movimientoIzq(i); 
			    break;
			    case 1: 
			    	movimientoArriba(i);
			    break;
			    case 2: 
			    	movimientoDer(i);
			    break;
			    case 3: 
			    	movimientoAbajo(i);
			    break;
			}
		}
		if(puntosMovimiento == 0 && contadorMovimiento == 0) {
			hayCambios = false;
		}
		
	}	
	private int dosOCuatro() 
	{
		Random aleatorio= new Random();
		int num=(aleatorio.nextInt(2));
		if(num==0)
			return 2;
		else
		    return 4;
	}
	private void posicionesLibres()
	{
		int n=-1;
		for (int fila=0; fila<this.matriz.length; fila++) 
		{
			
			for(int colu=0; colu<this.matriz.length; colu++) 
			{
				if(this.matriz[fila][colu].equals(0)){
					filaVacia.add(fila);
					columnaVacia.add(colu);
					n = 0;
				}
			}
		}
		
	}
	private void posicionAleatorio() 
	{
			int num = filaVacia.size()-1;
			Random aleatorio = new Random();
			if(num == 0) {
				indiceVacio = 0;
			}else {
				indiceVacio = (aleatorio.nextInt(num));
			}
	}	
    void colocarAleatorio(){
    	filaVacia = new ArrayList<Integer>();
		columnaVacia = new ArrayList<Integer>();
		posicionesLibres();
		posicionAleatorio();
		
		int indiceFila = (int) filaVacia.get(this.indiceVacio);
		int indiceColumna = (int) columnaVacia.get(this.indiceVacio);
		this.matriz[indiceFila][indiceColumna] = dosOCuatro();
	}
    private void inicializarVariables() {
    	unionesMovimiento  = 0;
		puntosMovimiento   = 0;
		contadorMovimiento = 0;
		hayCambios = true;
    }

}
