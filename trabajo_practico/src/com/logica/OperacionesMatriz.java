package com.logica;

public final class OperacionesMatriz {
	
	
	
	public static boolean compararMatriz(Integer[][] matriz2,Integer[][] matriz1) {
		boolean iguales = true;
		for(int i = 0;i<matriz1.length;i++) {
			for(int j = 0;j<matriz1.length;j++) {
				if(!matriz1[i][j].equals(matriz2[i][j])) {
					return false;
				}
			}
		}
		return iguales;
	}
	public static Integer[][] cargarMatriz(Integer[][] matrizOrigen) {
		Integer [][] matrizDestino = new Integer[4][4];
		for(int i = 0;i<matrizOrigen.length;i++) {
			for(int j = 0;j<matrizOrigen.length;j++) {
				matrizDestino[i][j] = matrizOrigen[i][j];
			}
		}
		return matrizDestino;
	}
	public static Integer[][] crearMatriz(){
		Integer[][] matriz = {{new Integer(0),new Integer(0),new Integer(0),new Integer(0)},
						  {new Integer(0),new Integer(0),new Integer(0),new Integer(0)},
						  {new Integer(0),new Integer(0),new Integer(0),new Integer(0)},
						  {new Integer(0),new Integer(0),new Integer(0),new Integer(2)}};
		return matriz;
	}

	public static boolean matrizLlena(Integer [][] matriz) {
		boolean llena = true;
		for(int i = 0;i<matriz.length;i++) {
			for(int j = 0;j<matriz.length;j++) {
				if(matriz[i][j].equals(0)) {
					return false;
				}
			}
		}
		return llena;
	}
}
