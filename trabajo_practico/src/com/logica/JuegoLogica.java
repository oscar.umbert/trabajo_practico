package com.logica;

import java.util.ArrayList;
import java.util.List;

public class JuegoLogica {
	private Tablero tablero;
	private boolean finalizo;
	private boolean gano;
	private Integer puntaje;
	private int[] listaHistorica;
	public boolean isFinalizo() {
		return finalizo;
	}
	public boolean isGano() {
		return gano;
	}
	public Integer getPuntaje() {
		return puntaje;
	}
	public int[] getListaHistorica() {
		return listaHistorica;
	}
	public Integer[][] obtenerMatriz(){
		return tablero.getMatriz();
	}
	public JuegoLogica() {
		super();
		tablero = new Tablero();
		finalizo = false;
		gano = false;
		puntaje = 0;
		listaHistorica = new int[] {0,0,0,0,0,0};
		
	}
	public JuegoLogica(Integer[][] matriz) {
		super();
		tablero = new Tablero(matriz);
		finalizo = false;
		gano = false;
		puntaje = 0;
		listaHistorica = new int[] {0,0,0,0,0,0};
		
	}
	public void reiniciar() {
		tablero = new Tablero();
		finalizo = false;
		gano = false;
		puntaje = 0;
	}
	public Integer[][] aplicarMovimiento(int sentido){
		
		tablero.calcularMovimiento(sentido);
		puntaje = puntaje + tablero.getPuntosMovimiento();
		
		if(tablero.getGano()) {
			finalizo = true;
			gano = true;
		}
		if(tablero.hayCambios()){
			tablero.colocarAleatorio();
		}
		
		return tablero.getMatriz();
			
	}
	
	public void agregarPuntaje(){
		int numero = this.puntaje;
		int auxiliar = 0;
		for(int i=0 ;i<listaHistorica.length;i++){
			
			if(listaHistorica[i]<numero){
				auxiliar = listaHistorica[i];
				listaHistorica[i]= numero;
				numero = auxiliar;
			}
        
		}
	}	

	public int sugerirMovimiento() {
		List<Integer> puntosMovimientos = new ArrayList<Integer>();
		List<Integer> unionesMovimientos = new ArrayList<Integer>();
		Integer[][] matriz;
		Tablero tableroLocal;
		int contadorIguales = 0;
		for(int i = 0;i<2;i++) {
			
			matriz = OperacionesMatriz.cargarMatriz(tablero.getMatriz());
			tableroLocal = new Tablero(matriz);
			tableroLocal.calcularMovimiento(i);
			puntosMovimientos.add(tableroLocal.getPuntosMovimiento());
			unionesMovimientos.add(tableroLocal.getUnionesMovimiento());
			
			if(OperacionesMatriz.compararMatriz(matriz,tablero.getMatriz()) && OperacionesMatriz.matrizLlena(matriz)) {
				contadorIguales++;
			}
		}
		if(contadorIguales == 2) {
			finalizo = true;
		}

		return calcularMayor(puntosMovimientos,unionesMovimientos);
		
		
	}	
	private int calcularMayor(List<Integer> puntosMovimientos, 
							  List<Integer> unionesMovimientos) {
		int indiceMayor = 0;
		
		if(unionesMovimientos.get(1) > unionesMovimientos.get(0)) {
			indiceMayor = 1;
		}else if(unionesMovimientos.get(0) == unionesMovimientos.get(1)) {
			if(puntosMovimientos.get(1) > puntosMovimientos.get(0) ) {
				indiceMayor = 1;
			}
		}
		
		return indiceMayor;
	}
	
	
}
