package com.interfaz;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Color;
import java.awt.Font;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.awt.event.ActionEvent;
import javax.swing.ImageIcon;
import java.awt.Toolkit;

public class MainWindow extends JFrame {

	private JPanel contentPane;
	
	
	

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainWindow ventana = new MainWindow();
					ventana.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	

	
	public MainWindow() {
		setIconImage(Toolkit.getDefaultToolkit().getImage("IconoGame.png"));
		setForeground(Color.ORANGE);
		setFont(new Font("Arial Black", Font.PLAIN, 12));
		setBackground(Color.BLACK);
		setTitle("2048Game");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(450, 200, 470, 297);
		
		contentPane = new JPanel();
		contentPane.setForeground(Color.BLACK);
		contentPane.setBackground(new Color(244, 164, 96));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		botonJugar();
		botonSalir();	
		labels();	
	}
	
	private void labels(){
		JLabel lblTitulo = new JLabel(" 2048 GAME");
		lblTitulo.setForeground(new Color(139, 0, 0));
		lblTitulo.setFont(new Font("Freestyle Script", Font.ITALIC, 40));
		lblTitulo.setBackground(Color.RED);
		lblTitulo.setBounds(143, 53, 278, 67);
		contentPane.add(lblTitulo);
		//*********************************************************
		
		JLabel lblBienvenido = new JLabel("");
		lblBienvenido.setIcon(new ImageIcon("Bienvenido.png"));
		lblBienvenido.setBounds(199, 11, 81, 43);
		contentPane.add(lblBienvenido);	
	}
	
	private void botonJugar(){
		JButton Boton_JUGAR = new JButton("JUGAR");
		Boton_JUGAR.setIcon(new ImageIcon(("JostickMain.png")));
		
		Juego ventanaJuego = new Juego();
		Boton_JUGAR.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ventanaJuego.setVisible(true);
				dispose();//Pisa la ventana anterior
				
			}
		});
		Boton_JUGAR.setFont(new Font("Freestyle Script", Font.PLAIN, 22));
		Boton_JUGAR.setForeground(Color.BLACK);
		Boton_JUGAR.setBackground(new Color(105, 105, 105));
		Boton_JUGAR.setBounds(36, 140, 143, 34);
		contentPane.add(Boton_JUGAR);
	}	
	private void botonSalir(){	
		//*********************************************************
		JButton Boton_SALIR = new JButton("SALIR");
		Boton_SALIR.setIcon(new ImageIcon("SalirMain.png"));
		
		Boton_SALIR.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {	
				System.exit(0);
			}
		});
		
		Boton_SALIR.setBackground(Color.GRAY);
		Boton_SALIR.setForeground(Color.BLACK);
		Boton_SALIR.setFont(new Font("Freestyle Script", Font.PLAIN, 22));
		Boton_SALIR.setBounds(268, 140, 153, 34);
		contentPane.add(Boton_SALIR);	
	
	}	
}
