package com.interfaz;
import java.awt.EventQueue;


import javax.swing.JFrame;
import javax.swing.JPanel;

import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;

import com.logica.JuegoLogica;
import java.awt.Color;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;


public class Juego extends JFrame {

	private JPanel contentPane;
	private JPanel panel;
	private JTable tablaJuego;
	private Integer [][] matriz = new Integer[4][4];
	private JuegoLogica juegoLogica;
	private JLabel puntajeNumero;
	private JLabel labelIzq;
	private JLabel labelDer;
	private JLabel labelArriba;
	private JLabel labelAbajo;
	private JLabel lblGanaste;
	private JLabel lblPerdiste;
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Juego frame = new Juego();
					frame.setVisible(true);
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	/**
	 * Create the frame.
	 */
	@SuppressWarnings("serial")
	public void inicializarTablero() {	
		panel = new JPanel();
		panel.setBounds(57,61,300, 300);
		FormatoTabla formatoTabla = new FormatoTabla();
		contentPane.add(panel);

		tablaJuego = new JTable();
		tablaJuego.setEnabled(false);
		tablaJuego.setToolTipText("");
		tablaJuego.setBorder(new LineBorder(new Color(153, 153, 0), 3));
		tablaJuego.setBackground(Color.WHITE);
		tablaJuego.setFont(new Font("Tahoma", Font.BOLD, 20));
		tablaJuego.setRowHeight(60);
		panel.add(tablaJuego);
		tablaJuego.setModel(new DefaultTableModel(
			new Object[][] {
				{"", "", "", ""},
				{"", "", "", ""},
				{"", "", "", ""},
				{"", "", "", ""},
			},
			new String[] {
				"New column", "New column", "New column", "New column"
			}
		) {
			@SuppressWarnings("rawtypes")
			Class[] columnTypes = new Class[] {
				String.class, String.class, String.class, String.class
			};
			public Class getColumnClass(int columnIndex) {
				return columnTypes[columnIndex];
			}
		});
		
		tablaJuego.getColumnModel().getColumn(0).setPreferredWidth(60);
		tablaJuego.getColumnModel().getColumn(0).setMinWidth(60);
		tablaJuego.getColumnModel().getColumn(1).setPreferredWidth(60);
		tablaJuego.getColumnModel().getColumn(2).setPreferredWidth(60);
		tablaJuego.getColumnModel().getColumn(3).setPreferredWidth(60);
	
		tablaJuego.setDefaultRenderer(Object.class,formatoTabla);		
	}
	
	public void inicializar() {
		
		puntajeNumero = new JLabel("0");
		matriz = juegoLogica.obtenerMatriz();
		
		inicioPanel();
		inicializarFlechas();
		inicializarTablero();		
		labelPuntaje();
		teclas();
		dibujarTablero();
		sugerirCambio();
		
	}		
	private void labelPuntaje(){
		JLabel lblPuntaje = new JLabel("Puntaje");
		lblPuntaje.setForeground(Color.black);
		lblPuntaje.setFont(new Font("Arial Black", Font.PLAIN, 20));
		lblPuntaje.setBounds(425, 51, 90, 31);
		contentPane.add(lblPuntaje);
		
	}
	private void inicioPanel(){
		setIconImage(Toolkit.getDefaultToolkit().getImage("IconoGame.png"));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(450, 200, 591, 460);
		
		//Confirmacion de cierre de JFrame
		try{	
		this.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		addWindowListener(new WindowAdapter(){
			public void windowClosing(WindowEvent e){
				confirmarSalida();		
		}
		});	
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		//****************************************
		
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(null);
		setContentPane(contentPane);	
	}				
	private void teclas(){	
		
		{	
			contentPane.setFocusable(true);
			contentPane.requestFocus();
			contentPane.addKeyListener(new KeyAdapter(){	
				
				@Override
				public void keyPressed(KeyEvent e) {			
					if (e.getKeyCode()==KeyEvent.VK_UP ){				
						matriz = juegoLogica.aplicarMovimiento(1);
						dibujarTablero();
						sugerirCambio();
					}if (KeyEvent.VK_DOWN==e.getKeyCode()){
						matriz = juegoLogica.aplicarMovimiento(3);
						dibujarTablero();
						sugerirCambio();
					}if (KeyEvent.VK_RIGHT==e.getKeyCode()){
						matriz = juegoLogica.aplicarMovimiento(2);
						dibujarTablero();
						sugerirCambio();
					}if (KeyEvent.VK_LEFT==e.getKeyCode()) {
						matriz = juegoLogica.aplicarMovimiento(0);
						dibujarTablero();
						sugerirCambio();
					}if(e.getKeyCode()==KeyEvent.VK_ESCAPE){
						confirmarSalida();
					}
				}
				
			});		
			
		}
	}
	
	
	private void inicializarFlechas() {
		labelIzq = new JLabel();
		labelIzq.setIcon(new ImageIcon("izquierda.png"));
		labelIzq.setBounds(60, 178, 26, 16);
		labelIzq.setVisible(false);
		contentPane.add(labelIzq);
		
		labelDer = new JLabel();
		labelDer.setIcon(new ImageIcon("derecha.png"));
		labelDer.setBounds(330,176, 35, 16);
		labelDer.setVisible(false);
		contentPane.add(labelDer);
		
		labelArriba = new JLabel();
		labelArriba.setIcon(new ImageIcon("arriba.png"));
		labelArriba.setBounds(195,40,20,22);
		labelArriba.setVisible(false);
		contentPane.add(labelArriba);
		
		labelAbajo = new JLabel();
		labelAbajo.setIcon(new ImageIcon("abajo.png"));
		labelAbajo.setBounds(195, 315, 20,22);
		labelAbajo.setVisible(false);
		contentPane.add(labelAbajo);
		
	}
	
	public Juego() {
		setResizable(false);
		juegoLogica = new JuegoLogica();	
		inicializar();
		
	}
	public Juego(JuegoLogica juego) {
		setResizable(false);
		juegoLogica = juego;
		inicializar();
		
	}
	private void sugerirCambio() {
		int sentido = juegoLogica.sugerirMovimiento();
		if(sentido == 0) {
			labelIzq.setVisible(true);
			labelDer.setVisible(true);
			labelArriba.setVisible(false);
			labelAbajo.setVisible(false);
		}else {
			labelArriba.setVisible(true);
			labelAbajo.setVisible(true);
			labelIzq.setVisible(false);
			labelDer.setVisible(false);
			
		}	
	}
	
	private void visualizarFlechas(){
		if(juegoLogica.isFinalizo() && juegoLogica.isGano()) {	
			lblGanaste.setVisible(true);
			
		}else if(juegoLogica.isFinalizo() && !juegoLogica.isGano()){
			lblPerdiste.setVisible(true);
		}
	}
	
	private void colocarBotonReiniciar(){
		JButton btnIniciar = new JButton("Reiniciar");
		btnIniciar.setIcon(new ImageIcon("Refresh.png"));
		btnIniciar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				juegoLogica.agregarPuntaje();
				juegoLogica.reiniciar();
				inicializar();
				
			}
		});
		btnIniciar.setBounds(418, 9, 120, 25);
		contentPane.add(btnIniciar);		
		
	}
	
	private void puntaje(){
		puntajeNumero.setForeground(Color.RED);
		puntajeNumero.setBackground(Color.DARK_GRAY);
		puntajeNumero.setBounds(428, 95, 77, 31);
		puntajeNumero.setFont(new Font ("Arial Black", Font.PLAIN,20));
		puntajeNumero.setText(juegoLogica.getPuntaje().toString());
		contentPane.add(puntajeNumero);		
		
	}
	
	
	private void imgPerdisteOGanaste(){
		lblGanaste = new JLabel("");
		lblGanaste.setIcon(new ImageIcon("ganaste.gif"));
		lblGanaste.setBounds(358, 180, 217, 146);
		lblGanaste.setVisible(false);
		
		contentPane.add(lblGanaste);
		
		
		
		lblPerdiste = new JLabel("");
		
		lblPerdiste.setIcon(new ImageIcon("perdiste.gif"));
		lblPerdiste.setBounds(320, 180, 217, 146);
		lblPerdiste.setVisible(false);
		
		contentPane.add(lblPerdiste);
		
		
		

		
	}
	
	private void dibujarTablero() {
		
		puntaje();
		colocarBotonReiniciar();
		visualizarFlechas();
		cargarTabla();
		imgPerdisteOGanaste();
		botonRecords();
		
	}
	private void confirmarSalida(){
		int msj = JOptionPane.showConfirmDialog(null,"¿Quieres salir?");	
		if (msj == JOptionPane.YES_NO_OPTION){
			System.exit(1);}

	}
	
	
	private void botonRecords(){
		JButton button = new JButton("RECORDS");
		button.setIcon(new ImageIcon("trofeo.png"));
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Records historial = new Records(juegoLogica);
				historial.setVisible(true);
				Juego.this.dispose();
			}
		});
		button.setToolTipText("RECORDS");
		button.setForeground(Color.BLACK);
		button.setFont(new Font("Freestyle Script", Font.PLAIN, 22));
		button.setBackground(Color.GREEN);
		button.setBounds(394, 340, 143, 34);
		
		contentPane.add(button);
	}
	
	private void  cargarTabla() {
		for(int i = 0;i<matriz.length;i++) {
			for(int j = 0 ;j<matriz.length;j++) {
				
				if(matriz[i][j].equals(0)) {
					tablaJuego.getModel().setValueAt("",i,j);
				}else {
					tablaJuego.getModel().setValueAt(matriz[i][j], i, j);
				}
			}
		}
	}
}
	